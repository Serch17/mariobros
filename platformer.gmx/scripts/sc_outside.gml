with (obj_player){
    if(obj_player.x > room_width){
        obj_player.x = 0; 
    }else if(obj_player.x < 0){
        obj_player.x = room_width;
    }
}


with (obj_enemy){
    if(obj_enemy.y < 525){
        if(obj_enemy.x > room_width){
            obj_enemy.x = 0; 
        }else if(obj_enemy.x < 0){
            obj_enemy.x = room_width;
        }
    }else{
        if(obj_enemy.x > room_width){
            obj_enemy.x = 0; 
            obj_enemy.y = 190; 
        }else if(obj_enemy.x < 0){
            obj_enemy.x = room_width;
            obj_enemy.y = 190; 
        }
    }
}
