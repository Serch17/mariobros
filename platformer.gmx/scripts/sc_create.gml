grav = 0.27;
hsp = 0;
vsp = 0;

jumpspeed = 8;
movespeed = 3;

//Constants
grounded = false;
jumping = false;
global.stunned = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 3;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 12;
hkp_count_big = 17;

//init variables
key_left = 0;
key_right = 0;
key_jump = false;
